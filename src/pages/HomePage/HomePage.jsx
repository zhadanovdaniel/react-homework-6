
import HomeBody from "./HomeBody";
import React, { useState, useEffect } from "react";
import CardWrapper from "../../components/CardWrapper/CardWrapper";
import TablesWrapper from "../../components/TablesWrapper/TablesWrapper";
//має бути три масиви для фейворіта для кошика і продактс як стейти
export default function HomePage(props) {
  
const {products, setIsAdded, setActiveStarsCount,isModalOpen,openModal,closeModal,activeArticle, setActiveArticle,handleCartClick, viewMode} = props;
  return (
    <>
      <div className="HomePage">
      
        <HomeBody>
          {viewMode ?  <CardWrapper
            products={products}
            setIsAdded={setIsAdded}
            setActiveStarsCount={setActiveStarsCount}
            isModalOpen={isModalOpen}
            openModal={openModal}
            closeModal={closeModal}
            activeArticle={activeArticle}
            setActiveArticle={setActiveArticle}
            handleCartClick={handleCartClick}
          /> :  <TablesWrapper
          products={products}
          setIsAdded={setIsAdded}
          setActiveStarsCount={setActiveStarsCount}
          isModalOpen={isModalOpen}
          openModal={openModal}
          closeModal={closeModal}
          activeArticle={activeArticle}
          setActiveArticle={setActiveArticle}
          handleCartClick={handleCartClick}
        />}
       


         
        </HomeBody>
      </div>
    </>
  );
}
