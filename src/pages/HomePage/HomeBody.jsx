import { useContext } from "react"
import { Context } from "../../App"

export default function HomeBody({children}){


    const [viewMode, toggleViewMode] = useContext(Context)
    return(
            <>
         
            <button onClick={()=>toggleViewMode(!viewMode)}>{viewMode ? "Перейти до таблиці" : "Перейти до карток"}</button>
            <h3>{children}</h3>
            
            </>

        
    )




}