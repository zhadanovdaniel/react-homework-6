import { productReducer } from './productsReducer';
import { FETCH_PRODUCTS_REQUEST, FETCH_PRODUCTS_SUCCESS, FETCH_PRODUCTS_FAILURE } from '../actions/types';

describe('productReducer', () => {
  it('should return the initial state', () => {
    expect(productReducer(undefined, {})).toEqual({
      products: [],
      loading: false,
      error: null
    });
  });

  it('should handle FETCH_PRODUCTS_REQUEST', () => {
    const action = { type: FETCH_PRODUCTS_REQUEST };
    expect(productReducer(undefined, action)).toEqual({
      products: [],
      loading: true,
      error: null
    });
  });

  it('should handle FETCH_PRODUCTS_SUCCESS', () => {
    const productsData = [{ id: 1, name: 'Product 1' }, { id: 2, name: 'Product 2' }];
    const action = { type: FETCH_PRODUCTS_SUCCESS, payload: productsData };
    expect(productReducer(undefined, action)).toEqual({
      products: productsData,
      loading: false,
      error: null
    });
  });

  it('should handle FETCH_PRODUCTS_FAILURE', () => {
    const error = 'Error fetching products';
    const action = { type: FETCH_PRODUCTS_FAILURE, payload: error };
    expect(productReducer(undefined, action)).toEqual({
      products: [],
      loading: false,
      error: error
    });
  });

  it('should return current state for unknown action types', () => {
    const currentState = {
      products: [{ id: 1, name: 'Product 1' }],
      loading: false,
      error: null
    };
    const action = { type: 'UNKNOWN_ACTION' };
    expect(productReducer(currentState, action)).toEqual(currentState);
  });
});
