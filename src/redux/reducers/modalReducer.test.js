import { modalReducer } from './modalReducer';
import { OPEN_MODAL, CLOSE_MODAL } from '../actions/types';

describe('modalReducer', () => {
  it('should return the initial state', () => {
    expect(modalReducer(undefined, {})).toEqual({
      isModalOpen: false
    });
  });

  it('should handle OPEN_MODAL', () => {
    const action = { type: OPEN_MODAL };
    expect(modalReducer(undefined, action)).toEqual({
      isModalOpen: true
    });
  });

  it('should handle CLOSE_MODAL', () => {
    const action = { type: CLOSE_MODAL };
    expect(modalReducer(undefined, action)).toEqual({
      isModalOpen: false
    });
  });

  it('should return current state for unknown action types', () => {
    const currentState = {
      isModalOpen: true
    };
    const action = { type: 'UNKNOWN_ACTION' };
    expect(modalReducer(currentState, action)).toEqual(currentState);
  });
});
