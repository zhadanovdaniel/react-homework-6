import axios from "axios";

import {
  FETCH_PRODUCTS_REQUEST,
  FETCH_PRODUCTS_SUCCESS,
  FETCH_PRODUCTS_FAILURE,
  OPEN_MODAL,
  CLOSE_MODAL,
} from "./types";

export function fetchProducts() {
  return (dispatch) => {
    dispatch({ type: FETCH_PRODUCTS_REQUEST });
    axios
      .get("../../../public/products.json")
      .then((response) => {
        dispatch({
          type: FETCH_PRODUCTS_SUCCESS,
          payload: response.data,
        });
      })
      .catch((error) => {
        dispatch({
          type: FETCH_PRODUCTS_FAILURE,
          payload: "Помилка завантаження даних про товари.",
        });
      });
  };
}
export function openModal() {
  return {
    type: OPEN_MODAL,
  };
}

export function closeModal() {
  return {
    type: CLOSE_MODAL,
  };
}
