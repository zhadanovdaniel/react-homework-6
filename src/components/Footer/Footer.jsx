import React from 'react';
import './Footer.scss'; // Підключаємо файл стилів для футера

const Footer = () => {
  return (
    <footer className="footer">
      <div className="footer__container">
        <div className="footer__logo">
          <img src="src\assets\logo.jpeg" alt="Логотип магазина" />
          <h1>Footbolka.com</h1>
        </div>
        <div className="footer__links">
          <h3>Швидкий доступ</h3>
          <ul>
            <li><a href="#">Головна</a></li>
            <li><a href="#">Каталог</a></li>
            <li><a href="#">Про нас</a></li>
          </ul>
         
        </div>
        <div className="footer__social">
          <h3>Ми в соціальних мережах</h3>
          <ul>
            <li><a href="#"><i className="fab fa-facebook">facebook </i></a></li>
            <li><a href="#"><i className="fab fa-instagram">instagram</i></a></li>
            <li><a href="#"><i className="fab fa-twitter">twitter</i></a></li>
          </ul>
        </div>
      </div>
      <div className="footer__newsletter">
    
        
      </div>
      <div className="footer__payment-methods">
        <h3>Способи оплати</h3>
        <ul>
          <li><img className="footer__payment-methods-logo" src="src\assets\visa.png" alt="Visa" /></li>
          <li><img className="footer__payment-methods-logo" src="src\assets\mastercard.png" alt="Mastercard" /></li>
          <li><img className="footer__payment-methods-logo" src="src\assets\paypal.png" alt="PayPal" /></li>
        </ul>
      </div>
      <div className="footer__copyright">
        <p>&copy; {new Date().getFullYear()} Footbolka.com. Усі права захищені.</p>
      </div>
    </footer>
  );
};

export default Footer;
