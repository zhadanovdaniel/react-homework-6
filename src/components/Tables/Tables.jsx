import React, { useState, useEffect } from "react";
import "./Tables.scss";

export default function Tables(props) {
  const {
    src,
    alt,
    productName,
    productPrice,
    productArticle,
    productColor,
    setActiveStarsCount,
    ModalOpen,
    setActiveArticle,
    activeCart,
    activeStar,
    cartTittle
  } = props;

  const [fav, setFav] = useState(false); // стан обраного товару
  const [cart, setCart] = useState(false); // стан товару, який додано до кошика

  const handleStarClick = () => {
    const updatedFav = !fav;
    setFav(updatedFav);
    setActiveStarsCount((prevCount) => prevCount + (updatedFav ? 1 : -1));

    // Збереження стану обраного товару в localStorage
    const favorites = JSON.parse(localStorage.getItem("favorites")) || [];
    if (updatedFav) {
      localStorage.setItem(
        "favorites",
        JSON.stringify([...favorites, productArticle])
      );
    } else {
      localStorage.setItem(
        "favorites",
        JSON.stringify(favorites.filter((article) => article !== productArticle))
      );
    }
  };

  const handleCartClick = () => {
    ModalOpen();
    setActiveArticle(productArticle);

    // Додавання товару до кошика та збереження в localStorage
    const addedToCart = JSON.parse(localStorage.getItem("addedToCart")) || [];
    if (!cart) {
      localStorage.setItem(
        "addedToCart",
        JSON.stringify([...addedToCart, productArticle])
      );
    } else {
      localStorage.setItem(
        "addedToCart",
        JSON.stringify(addedToCart.filter((article) => article !== productArticle))
      );
    }
    setCart(!cart);
  };

  useEffect(() => {
    // Завантаження стану обраного товару з localStorage
    const favorites = JSON.parse(localStorage.getItem("favorites")) || [];
    setFav(favorites.includes(productArticle));

    // Завантаження стану товару в кошику з localStorage
    const addedToCart = JSON.parse(localStorage.getItem("addedToCart")) || [];
    setCart(addedToCart.includes(productArticle));
  }, [productArticle]);

  return (
    <tr className="tables">
      <td>
        <img src={src} alt={alt} />
      </td>
      <td>{productName}</td>
      <td>{productPrice} грн</td>
      <td>Артикул: {productArticle}</td>
      <td>Колір: {productColor}</td>
      <td>
        {activeCart && (
          <button
            className={"shoping_cart"}
            data-testid="button"
            onClick={handleCartClick}
          >
            {cartTittle}
          </button>
        )}
        {activeStar && (
          <div
            data-testid="star-button"
            className={`star ${fav ? "active" : ""}`}
            onClick={handleStarClick}
          ></div>
        )}
      </td>
    </tr>
  );
}
