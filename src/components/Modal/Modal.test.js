import { render, fireEvent } from '@testing-library/react';
import Modal from './Modal';
import "@testing-library/jest-dom";

describe('Modal component', () => {
  test('renders children when isOpen is true', () => {
    const onClose = jest.fn();
    const { getByText } = render(
      <Modal isOpen={true} onClose={onClose}>
        <div>Modal Content</div>
      </Modal>
    );


    expect(getByText('Modal Content')).toBeInTheDocument();
  });

  test('does not render children when isOpen is false', () => {
    const onClose = jest.fn();
    const { queryByText } = render(
      <Modal isOpen={false} onClose={onClose}>
        <div>Modal Content</div>
      </Modal>
    );

    
    expect(queryByText('Modal Content')).not.toBeInTheDocument();
  });

  test('calls onClose when clicking on modal overlay', () => {
    const onClose = jest.fn();
    const { getByTestId } = render(
      <Modal isOpen={true} onClose={onClose}>
        <div>Modal Content</div>
      </Modal>
    );

    fireEvent.click(getByTestId('modal-overlay'));

    expect(onClose).toHaveBeenCalledTimes(1);
  });

  test('does not call onClose when clicking on modal content', () => {
    const onClose = jest.fn();
    const { getByTestId } = render(
      <Modal isOpen={true} onClose={onClose}>
        <div>Modal Content</div>
      </Modal>
    );

    
    fireEvent.click(getByTestId('modal'));

  
    expect(onClose).not.toHaveBeenCalled();
  });
});
