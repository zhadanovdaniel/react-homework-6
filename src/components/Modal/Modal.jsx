import "./Modal.scss"

export default function Modal({ isOpen, onClose, children }) {
  return (
    
    <>
      {isOpen ? (
        <div className="modal-overlay" data-testid="modal-overlay" onClick={onClose}>
          <div className="Modal" data-testid="modal" onClick={(e) => e.stopPropagation()}>
            {children}
          </div>
        </div>
      ) : null}
    </>
   
    
    
    )
 
  
}
 