import {render, fireEvent} from "@testing-library/react"
import "@testing-library/jest-dom";
import Button from "./Button"

// describe("<Button/>",()=> {
//     test('should always pass',()=>{
//     expect(2).toBe(2)
//     })

//     test('render component', ()=>{
//     render(<Button>Test</Button>)
//     expect(true).toBe(true)
//     })

//     test('schould show correct text', ()=> {
//          const button = render(<Button></Button>);
//         const text = button.getByText('YES, ADD TO CART');
//         expect(text).toBeInTheDocument();
//     })
// })

describe("<Button/>", () => {
    test('should always pass', () => {
      expect(2).toBe(2);
    });
  
    test('renders component with children', () => {
      const { getByText } = render(<Button>Test</Button>);
      expect(getByText('Test')).toBeInTheDocument();
    });
  
    test('renders button with specific classNames', () => {
      const { container } = render(<Button classNames="custom-class">Test</Button>);
      const button = container.querySelector('button');
      expect(button).toHaveClass('custom-class');
    });
  
    test('calls onClick when button is clicked', () => {
      const onClick = jest.fn();
      const { getByText } = render(<Button onClick={onClick}>Test</Button>);
      const button = getByText('Test');
      fireEvent.click(button);
      expect(onClick).toHaveBeenCalled();
    });
  
    
  });