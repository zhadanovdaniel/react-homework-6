import renderer from 'react-test-renderer';
import Button from './Button';

test('Button component renders correctly', () => {
  const tree = renderer
    .create(<Button classNames="btn-primary" onClick={() => {}}>Click me</Button>)
    .toJSON();
  expect(tree).toMatchSnapshot();
});