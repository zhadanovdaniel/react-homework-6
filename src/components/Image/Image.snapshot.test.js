import renderer from 'react-test-renderer';
import Image from './Image';

test('renders correctly', () => {
  const tree = renderer.create(<Image />).toJSON();
  expect(tree).toMatchSnapshot();
});
