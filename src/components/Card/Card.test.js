import {render, fireEvent} from "@testing-library/react"
import "@testing-library/jest-dom";
import Card from "./Card"




describe('Card component', () => {
  it('should trigger ModalOpen and setActiveArticle when shoping_cart button is clicked', () => {
    const ModalOpenMock = jest.fn();
    const setActiveArticleMock = jest.fn();

    const props = {
      src: 'image.jpg',
      alt: 'Product Image',
      productName: 'Product Name',
      productPrice: 100,
      productArticle: '123456',
      productColor: 'blue',
      setActiveStarsCount: jest.fn(),
      ModalOpen: ModalOpenMock,
      setActiveArticle: setActiveArticleMock,
      activeCart: true,
      activeStar: true,
      cartTittle: 'Add to Cart'
    };

    const { getByTestId } = render(<Card {...props} />);

    fireEvent.click(getByTestId('button'));

    expect(ModalOpenMock).toHaveBeenCalled();
    expect(setActiveArticleMock).toHaveBeenCalledWith('123456');
  });

  it('should toggle favorite status when star button is clicked', () => {
    const setActiveStarsCountMock = jest.fn();

    const props = {
      src: 'image.jpg',
      alt: 'Product Image',
      productName: 'Product Name',
      productPrice: 100,
      productArticle: '123456',
      productColor: 'blue',
      setActiveStarsCount: setActiveStarsCountMock,
      ModalOpen: jest.fn(),
      setActiveArticle: jest.fn(),
      activeCart: false,
      activeStar: true,
      cartTittle: 'Add to Cart'
    };

    const { getByTestId } = render(<Card {...props} />);

    fireEvent.click(getByTestId('star-button'));

    expect(setActiveStarsCountMock).toHaveBeenCalled();
  });
});
